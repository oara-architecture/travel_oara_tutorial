#!/usr/bin/env python3
import oara
import oara.actors

from std_msgs.msg import Float32
from oara_std_interfaces.msg import Float32Goal

class PayTaxiActor(oara.actors.Controller):

    def __init__(self, name: str):
        super().__init__(name, Float32Goal)
        self.add_data('cash', Float32)

    def on_activate(self, state):
        self.get_logger().info("Creating 'cash' publisher")
        self.cash_pub = self.create_publisher(Float32, "cash", 1)
        return super().on_activate(state)

    def on_deactivate(self, state):
        self.destroy_publisher(self.cash_pub)
        return super().on_deactivate(state)

    def select(self, goal_id, goal):
        _, cash = self.get_data('cash')
        self.get_logger().info(f"I have {cash.data}€")
        return cash.data >= goal.data

    def dispatch(self, goal_id, goal, plan):
        _, cash = self.get_data('cash')
        remaining = cash.data - goal.data
        self.cash_pub.publish(Float32(data=remaining))
        self.get_logger().info(f"Taxi paid; remaining cash: {remaining}")
        return True

    def monitor(self, goal_id):
        self.finish(goal_id)

    def drop(self, goal_id):
        return True

if __name__ == '__main__':
    oara.main(PayTaxiActor, "pay_taxi_actor")
#!/usr/bin/env python3
import enum
from collections import defaultdict
from typing import List

import oara
import oara.actors

from oara_std_interfaces.msg import Int32Goal, Float32Goal, StringGoal
from std_msgs.msg import Int32, String, Float32


class TaxiCompany(enum.Enum):
    YELLOW = "yellow"
    BLUE = "blue"


class TravelTaxiActor(oara.actors.POSActor):

    def __init__(self, name: str):
        super().__init__(name, Int32Goal)

        self.add_child_actor('call', StringGoal)
        self.add_child_actor('ride', Int32Goal)
        self.add_child_actor('pay', Float32Goal)
        self.add_data('loc', Int32)

        self.add_parameter(TaxiCompany.YELLOW.value+'-fixed-rate', float, "Fixed rate charged by Yellow Taxis", 1.0)
        self.add_parameter(TaxiCompany.YELLOW.value+'-km-rate', float, "Km rate charged by Yellow Taxis", 1.0)
        self.add_parameter(TaxiCompany.BLUE.value+'-fixed-rate', float, "Fixed rate charged by Blue Taxis", 4.0)
        self.add_parameter(TaxiCompany.BLUE.value+'-km-rate', float, "Km rate charged by Blue Taxis", 0.5)
        self.__selected_taxi : List[oara.UID, TaxiCompany] = defaultdict(list)

    def taxi_rate(self, company: TaxiCompany, distance: int) -> float:
        b = self.get_parameter(company.value+'-fixed-rate').value
        a = self.get_parameter(company.value+'-km-rate').value
        return a * distance + b

    def select(self, gid: oara.UID, goal: Int32):
        return True

    def expand(self, gid: oara.UID, goal: Int32):
        _, loc = self.get_data('loc')
        prices = [(self.taxi_rate(taxi, abs(loc.data - goal.data)), taxi) for taxi in TaxiCompany]
        self.get_logger().info(f"Taxi riding will cost {prices}€")
        min_fare, min_company = min(prices)
        max_fare, max_company = max(prices)

        if min_company not in self.__selected_taxi[gid]:
            price = min_fare
            company = min_company
            self.get_logger().info(f"Calling {min_company.name} Taxi!")
            self.__selected_taxi[gid].append(min_company)
        elif max_company not in self.__selected_taxi[gid]:
            price = max_fare
            company = max_company
            self.get_logger().info(f"Calling {max_company.name} Taxi!")
            self.__selected_taxi[gid].append(max_company)
        else:
            return False

        p = oara.Plan()
        p.add_sequence([
            (0, String(data=f'{company.name} TAXI!'), 'call'),
            (1, goal, 'ride'),
            (2, Float32(data=price), 'pay')
        ])
        p.write("travel-taxi-plan.dot")
        self.add_plan(gid, p, price)
        return True

    def evaluate(self, gid: oara.UID, report: oara.DispatchReport) -> oara.ResolveTo:
        if report.status and report.state == oara.GoalState.FINISHED:
            return oara.ResolveTo.CONTINUE

        elif report.status and report.actor == "call":
            return oara.ResolveTo.REPLAN

        else:
            return oara.ResolveTo.REFORM

if __name__ == '__main__':
    oara.main(TravelTaxiActor, "travel_taxi_actor")
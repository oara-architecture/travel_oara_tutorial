#!/usr/bin/env python3
import oara
import oara.actors

from std_msgs.msg import Int32
from oara_std_interfaces.msg import Int32Goal


class WalkActor(oara.actors.Controller):

    def __init__(self, name: str):
        super().__init__(name, Int32Goal)
        self.add_data('loc', Int32)

    def on_activate(self, state):
        self.get_logger().info("Creating 'loc' publisher")
        self.loc_pub = self.create_publisher(Int32, "loc", 1)
        return super().on_activate(state)

    def on_deactivate(self, state):
        self.destroy_publisher(self.loc_pub)
        return super().on_deactivate(state)

    def select(self, goal_id, goal):
        return True

    def dispatch(self, goal_id, goal, plan):
        header, value = self.get_data('loc')
        self.__loc = value.data
        self.get_logger().info(f"walking from {self.__loc} to {goal.data}")
        return True

    def monitor(self, goal_id):
        goal = self.get_goal(goal_id)
        self.get_logger().debug(f"current loc: {self.__loc}")
        distance = goal.data - self.__loc
        if distance == 0:
            self.get_logger().info(f"Arrived at location {goal}")
            self.finish(goal_id)
        else:
            if distance > 0:
                self.get_logger().info(f"one step forward...")
                self.__loc += 1
            else:
                self.get_logger().info(f"one step backward...")
                self.__loc -= 1
            self.loc_pub.publish(Int32(data=self.__loc))

    def drop(self, goal_id):
        return True


if __name__ == '__main__':
    oara.main(WalkActor, "walk_actor")
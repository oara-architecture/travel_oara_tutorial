#!/usr/bin/env python3
import oara
import oara.actors

from std_msgs.msg import Int32
from oara_std_interfaces.msg import Int32Goal


class RideTaxiActor(oara.actors.Controller):

    def __init__(self, name: str):
        super().__init__(name, Int32Goal)

    def on_activate(self, state):
        self.get_logger().info("Creating 'loc' publisher")
        self.loc_pub = self.create_publisher(Int32, "loc", 1)
        return super().on_activate(state)

    def on_deactivate(self, state):
        self.destroy_publisher(self.loc_pub)
        return super().on_deactivate(state)

    def select(self, goal_id, goal):
        return True

    def dispatch(self, goal_id, goal, plan):
        self.get_logger().info(f"Arrived at location {goal.data}")
        self.loc_pub.publish(goal)
        return True

    def monitor(self, goal_id):
        self.finish(goal_id)

    def drop(self, goal_id):
        return True


if __name__ == '__main__':
    oara.main(RideTaxiActor, "ride_taxi_actor")
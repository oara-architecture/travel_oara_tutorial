#!/usr/bin/env python3
import oara
import oara.actors

from oara_std_interfaces.msg import Int32Goal
from std_msgs.msg import Int32, Float32

from travel_tutorial_interfaces.msg import TravelDestination, TravelDestinationGoal

import travel_htn
from pyhop import hop

class TravelHTNActor(oara.actors.POSActor):

    def __init__(self, name: str):
        super().__init__(name, TravelDestinationGoal)

        self.add_child_actor('walk', Int32Goal)
        self.add_child_actor('taxi', Int32Goal)
        self.add_data('loc', Int32)
        self.add_data('cash', Float32)
        self.add_event("cash_found", oara.EventReaction.REPLAN)

    def on_activate(self, state):
        self.get_logger().info("Creating the HTN problem")
        hop.declare_operators(travel_htn.walk, travel_htn.call_taxi, travel_htn.ride_taxi, travel_htn.pay_driver)
        self.get_logger().info(f"operators: {', '.join(hop.get_operators())}")
        hop.declare_methods('travel', travel_htn.travel_by_foot, travel_htn.travel_by_taxi)
        self.get_logger().info(f"methods: {', '.join(hop.get_methods())}")
        return super().on_activate(state)

    def select(self, gid: oara.UID, goal: TravelDestination):
        return True

    def expand(self, gid: oara.UID, goal: TravelDestination):
        _, loc = self.get_data('loc')
        _, cash = self.get_data('cash')
        s = hop.State('s')
        s.loc = {'me': loc.data, 'taxi': -1}
        s.cash = {'me': cash.data}
        s.owe = {'me': 0}
        s.maximum_walk_distance = {'me': goal.maximum_walking_distance}

        htn_solution = hop.plan(s, [('travel', 'me', loc.data, goal.destination)],
            hop.get_operators(), hop.get_methods(),
            verbose=0)
        if not htn_solution:
            self.get_logger().error("No HTN plan found!")
            return False

        plan = oara.Plan()
        try:
            ride_goal = next((x[-1] for x in htn_solution if x[0] == 'ride_taxi'))
            self.get_logger().info(f"solution is to ride to {ride_goal}")
            plan.add_task(0, Int32(data=ride_goal), 'taxi')
        except StopIteration:
            try:
                walk_goal = next((x[-1] for x in htn_solution if x[0] == 'walk'))
                self.get_logger().info(f"solution is to walk to {walk_goal}")
                plan.add_task(0, Int32(data=walk_goal), 'walk')
            except StopIteration:
                self.get_logger().error("solution contains neither ride nor walk: HTN model error!")
                return False

        plan.write("travel-htn-plan.dot")
        self.add_plan(gid, plan, 0)
        return True

    def evaluate(self, gid: oara.UID, report: oara.DispatchReport) -> oara.ResolveTo:
        if report.status and report.state == oara.GoalState.FINISHED:
            return oara.ResolveTo.CONTINUE
        else:
            return oara.ResolveTo.REFORM


if __name__ == '__main__':
    oara.main(TravelHTNActor, "travel_htn_actor")
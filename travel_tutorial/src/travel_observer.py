#!/usr/bin/env python3
import oara
import oara.observers

from std_msgs.msg import Int32, Float32


class TravelObserver(oara.observers.Observer):

    def __init__(self, name: str):
        super().__init__(name)
        self.add_data('loc', Int32)
        self.add_data('cash', Float32)
        self.add_parameter('init_loc', oara.Parameter.Type.INTEGER,
                            'initial location', 0)
        self.add_parameter('init_cash', oara.Parameter.Type.DOUBLE,
                            'initial amount of cash', 100.0)

        self.add_data_monitor('cash_found', 'cash', 
                              r"pre{cash < 100} and {cash >= 100}")


    def on_configure(self, state):
        init_loc = self.get_parameter('init_loc').value
        self.get_logger().info(f"Initial location: {init_loc}")
        self.set_data('loc', Int32(data=init_loc))
        init_cash = self.get_parameter('init_cash').value
        self.get_logger().info(f"Initial cash: {init_cash}")
        self.set_data('cash', Float32(data=init_cash))
        return super().on_configure(state)

    def on_activate(self, state):
        self.get_logger().info("Creating ROS2 subscribers")
        self.loc_sub = self.create_subscription(Int32, "loc", self.loc_cb, 1)
        self.cash_sub = self.create_subscription(Float32, "cash", self.cash_cb, 1)
        return super().on_activate(state)

    def on_deactivate(self, state):
        self.destroy_subscription(self.loc_sub)
        self.destroy_subscription(self.cash_sub)
        return super().on_deactivate(state)

    def loc_cb(self, msg):
        self.get_logger().info(f"At location {msg.data}")
        self.set_data('loc', msg)

    def cash_cb(self, msg):
        self.get_logger().info(f"I have {msg.data}€")
        self.set_data('cash', msg)


if __name__ == '__main__':
    oara.main(TravelObserver, "travel_observer")
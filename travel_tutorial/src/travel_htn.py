### Functions ###

def taxi_rate(x, y):
    dist = abs(y - x)
    return (2 + 1 * dist)

### Operators ###

def walk(state, a, x, y):
    if state.loc[a] == x and abs(y - x) <= state.maximum_walk_distance[a]:
        state.loc[a] = y
        return state
    else:
        return False

def call_taxi(state, a, x):
    state.loc['taxi'] = x
    return state

def ride_taxi(state, a, x, y):
    if state.loc['taxi'] == x and state.loc[a] == x:
        state.loc['taxi'] = y
        state.loc[a] = y
        state.owe[a] = taxi_rate(x, y)
        return state
    else:
        return False

def pay_driver(state, a):
    if state.cash[a] >= state.owe[a]:
        state.cash[a] = state.cash[a] - state.owe[a]
        state.owe[a] = 0
        return state
    else:
        return False

### Methods ###

def travel_by_foot(state, a, x, y):
    return [('walk', a, x, y)]

def travel_by_taxi(state, a, x, y):
    if state.cash[a] >= taxi_rate(x, y):
        return [('call_taxi', a, x), ('ride_taxi', a, x, y), ('pay_driver', a)]
    return False

### Main ###

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-l', '--loc', type=int, default=0, help="initial location")
    parser.add_argument('-c', '--cash', type=int, default=10, help="initial cash")
    parser.add_argument('-m', '--maximum-walk-distance', type=int, default=3, help="maximum walking distance")
    parser.add_argument('-d', '--destination', type=int, default=13, help="destination")
    args = parser.parse_args()

    from pyhop import hop
    hop.declare_operators(walk, call_taxi, ride_taxi, pay_driver)
    hop.declare_methods('travel', travel_by_foot, travel_by_taxi)
    hop.print_operators(hop.get_operators())
    hop.print_methods(hop.get_methods())
    s = hop.State('s')
    s.loc = {'me': args.loc, 'taxi': -1}
    s.cash = {'me': args.cash}
    s.owe = {'me': 0}
    s.maximum_walk_distance = {'me': args.maximum_walk_distance}
    print("INITIAL STATE:")
    hop.print_state(s)
    hop.plan(s, [('travel', 'me', args.loc, args.destination)],
        hop.get_operators(), hop.get_methods(),
        verbose=2)
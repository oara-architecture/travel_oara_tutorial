from launch import LaunchDescription
from launch_ros.actions import LifecycleNode, Node

def generate_launch_description():

    call_actor = LifecycleNode(
        package='oara',
        executable='pub_sub_controller.py',
        name='call_taxi_actor',
        namespace="",
        output="both",
        arguments=["oara_std_interfaces/msg/StringGoal"],
        remappings=[('out', "taxi_call"), ('in', 'taxi_response')]
    )
    
    ride_taxi_actor = LifecycleNode(
        package='travel_tutorial',
        executable='ride_taxi_actor.py',
        name='ride_taxi_actor',
        namespace="",
        output="both")

    travel_observer = LifecycleNode(
        package='travel_tutorial',
        executable='travel_observer.py',
        name='travel_observer',
        namespace='',
        output="both",
        parameters=[{'init_cash': 50.0}])

    pay_taxi_actor = LifecycleNode(
        package='travel_tutorial',
        executable='pay_taxi_actor.py',
        name='pay_taxi_actor',
        namespace='',
        output="both",
        parameters=[{'cash.observer': 'travel_observer'}])

    walk_actor = LifecycleNode(
        package='travel_tutorial',
        executable='walk_actor.py',
        name='walk_actor',
        output="both",
        namespace="",
        parameters=[{'loc.observer': 'travel_observer'}])

    travel_taxi = LifecycleNode(
        package='travel_tutorial',
        executable='travel_taxi_actor.py',
        name='travel_taxi_actor',
        output="both",
        namespace="",
        parameters=[{
            'loc.observer': 'travel_observer',
            'call.actor': 'call_taxi_actor',
            'pay.actor': 'pay_taxi_actor',
            'ride.actor': 'ride_taxi_actor'
        }])

    travel_htn = LifecycleNode(
        package='travel_tutorial',
        executable='travel_htn_actor.py',
        name='travel_htn_actor',
        output="both",
        namespace="",
        parameters=[{
            'loc.observer': 'travel_observer',
            'cash.observer': 'travel_observer',
            'walk.actor': 'walk_actor',
            'taxi.actor': 'travel_taxi_actor',
            'cash_found.observer': 'travel_observer'
        }])

    lifecycle_manager = Node(
        package="nav2_lifecycle_manager",
        executable="lifecycle_manager",
        namespace="",
        name="lifecycle_manager",
        parameters=[{
            "autostart": True,
            "bond_timeout": 0.0,
            "node_names": [
                "call_taxi_actor",
                "ride_taxi_actor",
                "travel_observer",
                "pay_taxi_actor",
                "walk_actor",
                "travel_taxi_actor",
                "travel_htn_actor"
                ]
        }])

    return LaunchDescription([
        call_actor, ride_taxi_actor,
        travel_observer,
        pay_taxi_actor, walk_actor,
        travel_taxi, travel_htn,
        lifecycle_manager
        ])